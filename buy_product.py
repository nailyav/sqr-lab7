import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
update_inventory = "INSERT INTO Inventory(username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + excluded.amount"
get_inventory_size = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"

def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="password",
        host="localhost",
        port=5432
    ) 


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_stock, obj)

                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")
            try:
                cur.execute(get_inventory_size, obj)
                maybe_total = cur.fetchone()
                if maybe_total is None or maybe_total == (None,):
                    total = 0
                else:
                    total = maybe_total[0]
                if total + amount > 100:
                    raise Exception("Total amount of product should be less or equal to 100")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Unable to update inventory")

            cur.execute(update_inventory, obj)
            conn.commit()


buy_product('Alice', 'marshmello', 1)